$(function () {
    var APPLICATION_ID = "510DA428-3DDF-86EC-FF3E-88727A8A1700",
        SECRET_KEY = "CD74DFC9-3117-C10C-FF66-7877BB338200",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION); 
    
    var dataStore = Backendless.Persistence.of(Posts);
    var post = new Posts({title: "My First Blog Post", content: "My first blog content", authorEmail: "cdwyatt01@gmail.com"});
    dataStore.save(post);
});

function Post(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

